module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: ['src/js/*.js'],
      options: {
        forin: true,
        noarg: true,
        noempty: true,
        eqeqeq: true,
        bitwise: true,
        undef: true,
        unused: true,
        curly: true,
        browser: true,
        devel: true,
        jquery: true,
        indent: true,
        maxerr: 25,
        reporter: require('jshint-stylish')
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/js/<%= pkg.name %>.js',
        dest: 'build/jquery.<%= pkg.name %>.v<%= pkg.version %>.min.js'
      }
    },
//    cssmin: {
//      options: {
//        shorthandCompacting: false,
//        roundingPrecision: -1
//      },
//      target: {
//        files: {
//          'build/<%= pkg.name %>.css': ['src/css/<%= pkg.name %>.css']
//        }
//      }
//    },
    sass: {
      options: {
        sourcemap: 'none'
      },
      dist: {
        files: {
          'src/css/base.css': 'src/scss/base.scss'
        }
      }
    },
    watch: {
      sass: {
        files: ['src/scss/*.scss'],
        tasks: ['sass']
      },
      scripts: {
        files: ['src/js/*.js'],
        tasks: ['jshint', 'uglify']
      }
    },
    connect: {
      server: {
        options: {
          port: 9001
        }
      }
    },
    readme_generator: {
      gilmation : {
        options: {
          // Task-specific options go here.
          // detailed explanation is under options
          // Default options:
          readme_folder: "readme",
          output: "README.md",
          table_of_contents: true,
          generate_changelog: false,
          generate_footer: true,
          generate_title: true,
          informative: true,
          h1: "#",
          h2: "##",
          back_to_top_custom: null
        },
        order: {
          // Title of the piece and the File name goes here
          // "Filename" : "Title"
//          "installation.md": "Installation",
//          "usage.md": "Usage",
//          "options.md": "Options",
//          "example.md": "Example",
//          "output.md": "Example Output",
//          "building-and-testing.md": "Building and Testing",
//          "legal.md": "Legal Mambo Jambo"
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');

//  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['jshint', 'uglify', 'sass']);
  grunt.registerTask('serve', ['connect','watch']);
};
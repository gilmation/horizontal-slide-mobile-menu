;(function($){
  "use strict";
  $.horizontalSlideMobileMenu = function(element, options) {
    this.options = {};

    this.init = function(element, options) {
      this.options = $.extend({}, $.horizontalSlideMobileMenu.defaultOptions, options);
      if(typeof element.data('horizontalSlideMobileMenu') === 'undefined'){
        menuItemBehaviour(this.options);
        backButtonBehaviour(this.options);
        navOpenBehaviour(this.options);
        element.data('horizontalSlideMobileMenu', this);
      }
    };

    /* Public functions */

    this.init(element, options);
  };

  $.fn.horizontalSlideMobileMenu = function(options) {
    return this.each(function() {
      new $.horizontalSlideMobileMenu($(this), options);
    });
  };

  /* Private functions */

  function menuItemBehaviour(options){
    $('.item > a', options.targetElement).click(function() {
      var m_pos,
          c_new_name,
          c_name;
      if ($(this).parents('.item').first().find('.node').length > 0) {
        m_pos = $(this).parents('.node').length;
        if(m_pos > 1){
          c_name = options.classPrefix + (m_pos-1);
          $('> .node', options.targetElement).removeClass(c_name);
        }
        c_new_name = options.classPrefix + m_pos.length;
        $('> .node', options.targetElement).addClass(c_new_name);
        $(this).parents('.item').first().find('.node').first().addClass('active');
        return false;
      }
    });
    return true;
  }

  function backButtonBehaviour(options){
    $('#nav-back-link').click(function() {
      var  e_back = $('.node.active', options.targetElement),
        c_list,
        m_pos,
        c_name,
        parent_nodes = e_back.parentsUntil(options.targetElement,'.node');

      e_back.removeClass('active');
      if(parent_nodes.length < 1){
        $('body').toggleClass('nav-open');
        return;
      }else if(parent_nodes.length > 1){
        e_back.parents('.node').addClass('active');
      }

      c_list = $('> .node', options.targetElement).first().attr("class").split(" ");
      for (var i = 0; i < c_list.length; i++) {
        var re = new RegExp('^'+options.classPrefix);
        if (c_list[i].match(re)) {
          m_pos = c_list[i].replace(options.classPrefix, '');
          $('> .node', options.targetElement).first().removeClass(c_list[i]);
          if (m_pos > 1) {
            c_name = options.classPrefix + (m_pos - 1);
            $('> .node', options.targetElement).first().addClass(c_name);
            continue;
          }
        }
      }
    });
  }

  function navOpenBehaviour(options){
    $(options.targetNavOpen).click(function(){
      $('body').toggleClass('nav-open');
      $('html, body').animate({scrollTop: 0}, 200);
      return false;
    });
    return true;
  }
  /* Default options */
  $.horizontalSlideMobileMenu.defaultOptions = {
    targetElement: '#mobile-nav',
    targetNavOpen: '#mobile-nav-open',
    classPrefix : 'active-level-'
  };
})(jQuery);